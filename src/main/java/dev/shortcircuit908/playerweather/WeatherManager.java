package dev.shortcircuit908.playerweather;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Optional;
import java.util.UUID;

public class WeatherManager {
	private final PlayerWeatherPlugin plugin;
	private Integer lightning_task_id;
	private double strike_chance;
	
	public WeatherManager(PlayerWeatherPlugin plugin) {
		this.plugin = plugin;
	}
	
	public static void strikeFakeLightning(Player player) {
		// Get random positional offset
		int offset_x = Utils.RANDOM.nextInt(65) - 32;
		int offset_y = 0;
		int offset_z = Utils.RANDOM.nextInt(65) - 32;
		// Create a short night vision effect to simulate a lightning flash
		PotionEffect flash_effect = new PotionEffect(PotionEffectType.NIGHT_VISION, 4, 1, true, false, false);
		// Ensure player has metadata attached
		
		// Simple check if player can see the sky
		boolean can_see_sky = player.getLocation().getBlock().getLightFromSky() > 0;
		Location strike_pos = player.getLocation().add(offset_x, offset_y, offset_z);
		// Only play the "strike" sound if the player can see the sky
		if (can_see_sky) {
			player.playSound(strike_pos, Sound.ENTITY_LIGHTNING_BOLT_IMPACT,
					SoundCategory.WEATHER, 1.0f, Utils.RANDOM.nextFloat() / 2.0f
			);
		}
		player.playSound(strike_pos, Sound.ENTITY_LIGHTNING_BOLT_THUNDER,
				SoundCategory.WEATHER,
				10.0f, Utils.RANDOM.nextFloat() / 2.0f
		);
		// Only add the flash effect if the player can see the sky
		if (can_see_sky) {
			player.addPotionEffect(flash_effect);
		}
	}
	
	public void setPlayerWeather(UUID player_id, CustomWeatherType weather) {
		// Store custom weather type
		plugin.getStoredWeatherConfig().set(player_id.toString().toLowerCase(), weather.name());
		// If player is online, apply weather effects
		Player player = plugin.getServer().getPlayer(player_id);
		if (player != null) {
			applyPlayerWeather(player, weather);
		}
	}
	
	public void applyPlayerWeather(Player player, CustomWeatherType weather) {
		// Affix transient metadata to player
		player.setMetadata("custom_weather", new FixedMetadataValue(plugin, weather.name()));
		if (weather.resetToDefault()) {
			player.resetPlayerWeather();
		}
		else {
			player.setPlayerWeather(weather.getAmbientWeather());
		}
	}
	
	public void startLightningTask() {
		// Ensure only one task is started
		if (lightning_task_id != null) {
			throw new IllegalStateException("Lightning task already started");
		}
		// Minimum 10 ticks between strikes
		int min_ticks_between_strikes = Math.max(plugin.getConfig().getInt("lightning.min-ticks-between-strikes"), 10);
		// Strike chance >= 0.0 and <= 1.0
		strike_chance = Math.max(Math.min(1.0, plugin.getConfig().getDouble("lightning.strike-chance")), 0.0);
		lightning_task_id = plugin.getServer()
				.getScheduler()
				.scheduleSyncRepeatingTask(plugin, new LightningTask(), 0, min_ticks_between_strikes);
	}
	
	public void stopLightningTask() {
		if (lightning_task_id == null) {
			throw new IllegalStateException("Lightning task not started");
		}
		plugin.getServer().getScheduler().cancelTask(lightning_task_id);
		lightning_task_id = null;
	}
	
	private class LightningTask implements Runnable {
		@Override
		public void run() {
			if (Utils.RANDOM.nextDouble() <= strike_chance) {
				for (Player player : plugin.getServer().getOnlinePlayers()) {
					// No need for artificial lightning when the real deal is happening
					if (player.getWorld().isThundering()) {
						continue;
					}
					if (player.hasMetadata("custom_weather")) {
						Optional<MetadataValue> raw_weather_opt =
								player.getMetadata("custom_weather").stream().findFirst();
						if (raw_weather_opt.isPresent()) {
							// Parse attached weather type
							String raw_weather = raw_weather_opt.get().asString();
							CustomWeatherType weather_type = CustomWeatherType.get(raw_weather);
							// Only LIGHTNING and STORM have lightning effects
							if (weather_type.equals(CustomWeatherType.LIGHTNING) ||
									weather_type.equals(CustomWeatherType.STORM)) {
								strikeFakeLightning(player);
							}
						}
					}
				}
			}
		}
	}
}
