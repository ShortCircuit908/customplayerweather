package dev.shortcircuit908.playerweather;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.persistence.PersistentDataContainer;

import java.util.Optional;

public class WeatherListener implements Listener {
	private final PlayerWeatherPlugin plugin;
	
	public WeatherListener(PlayerWeatherPlugin plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void loadSavedWeather(final PlayerJoinEvent event) {
		Player player = event.getPlayer();
		// Get stored custom weather
		String weather_raw = plugin.getStoredWeatherConfig().getString(player.getUniqueId().toString().toLowerCase());
		// Default to server weather
		CustomWeatherType weather = CustomWeatherType.NONE;
		if (weather_raw != null) {
			weather = CustomWeatherType.get(weather_raw);
		}
		// Apply custom weather
		plugin.getWeatherManager().applyPlayerWeather(player, weather);
	}
	
	@EventHandler
	public void savePlayerWeather(final PlayerQuitEvent event) {
		Player player = event.getPlayer();
		// Get custom weather, if present
		Optional<MetadataValue> raw_weather_opt = player.getMetadata("custom_weather").stream().findFirst();
		if (raw_weather_opt.isPresent()) {
			String raw_weather = raw_weather_opt.get().asString();
			CustomWeatherType weather_type = CustomWeatherType.get(raw_weather);
			// Save custom weather
			plugin.getStoredWeatherConfig().set(player.getUniqueId().toString().toLowerCase(), weather_type.name());
		}
	}
	
	@EventHandler
	public void weatherPotion(final PlayerItemConsumeEvent event) {
		if (plugin.getConfig().getBoolean("weather-potions")) {
			Player player = event.getPlayer();
			ItemStack item = event.getItem();
			ItemMeta meta = item.getItemMeta();
			if (meta == null) {
				return;
			}
			// Get persistent metadata
			PersistentDataContainer container = meta.getPersistentDataContainer();
			if (container.has(plugin.getRecipeManager().getCustomWeatherTypeKey(),
					RecipeManager.CUSTOM_WEATHER_TAG)) {
				// Get custom weather tag
				CustomWeatherType type = container.get(plugin.getRecipeManager().getCustomWeatherTypeKey(),
						RecipeManager.CUSTOM_WEATHER_TAG
				);
				if (type == null) {
					return;
				}
				// Apply custom weather
				plugin.getWeatherManager().setPlayerWeather(player.getUniqueId(), type);
			}
		}
	}
}
