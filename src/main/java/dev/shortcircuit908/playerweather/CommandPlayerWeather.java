package dev.shortcircuit908.playerweather;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CommandPlayerWeather implements CommandExecutor, TabCompleter {
	private final PlayerWeatherPlugin plugin;
	private final String other_permission;
	
	public CommandPlayerWeather(PlayerWeatherPlugin plugin, String permission) {
		this.plugin = plugin;
		this.other_permission = permission + ".other";
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Too few arguments");
			return true;
		}
		if (args.length > 2) {
			sender.sendMessage(ChatColor.RED + "Too many arguments");
			return true;
		}
		
		OfflinePlayer target;
		CustomWeatherType weather;
		String target_name = "your";
		int weather_index = 0;
		// Command includes a username or UUID
		if (args.length == 2) {
			weather_index = 1;
			target = Utils.matchPlayerByNameOrId(args[0]);
			if (target == null) {
				sender.sendMessage(String.format(ChatColor.RED + "Player \"%1$s\" not found", args[0]));
				return true;
			}
			// If the target exists but doesn't have a name, use the supplied argument
			target_name = target.getName() == null ? args[0] : target.getName();
			target_name += target_name.endsWith("s") ? "'" : "'s";
		}
		else {
			// No username/UUID supplied, so the sender has to be a player
			if (!(sender instanceof OfflinePlayer)) {
				sender.sendMessage(ChatColor.RED + "Sender is not a player");
				return true;
			}
			target = (OfflinePlayer) sender;
		}
		
		// Parse weather type
		try {
			weather = CustomWeatherType.get(args[weather_index]);
		}
		catch (IllegalArgumentException e) {
			sender.sendMessage(
					String.format(ChatColor.RED + "Weather must be one of %1$s, %2$s, %3$s, %4$s, %5$s",
							(Object[]) CustomWeatherType.values()));
			return true;
		}
		if (!sender.equals(target) && !sender.hasPermission(other_permission)) {
			sender.sendMessage(ChatColor.RED + "You do not have permission to change another player's weather");
			return true;
		}
		// Apply custom weather
		plugin.getWeatherManager().setPlayerWeather(target.getUniqueId(), weather);
		sender.sendMessage(String.format("%3$sSet %4$s%1$s%3$s custom weather to %4$s%2$s", target_name,
				weather.name(), ChatColor.GOLD, ChatColor.AQUA));
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> suggestions = new ArrayList<>();
		// Suggest weather types first
		for (CustomWeatherType type : CustomWeatherType.values()) {
			String name = type.name().toLowerCase();
			if (name.startsWith(args[args.length - 1].toLowerCase())) {
				suggestions.add(name);
			}
		}
		if (sender.hasPermission(other_permission)) {
			// Suggest player names if completing the first argument
			if (args.length == 1) {
				for (Player player : plugin.getServer().getOnlinePlayers()) {
					if (player.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
						suggestions.add(player.getName());
					}
				}
			}
		}
		return suggestions;
	}
}
