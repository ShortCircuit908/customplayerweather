package dev.shortcircuit908.playerweather;

import org.bukkit.WeatherType;

public enum CustomWeatherType {
	NONE("server", "reset", "default"),
	CLEAR(WeatherType.CLEAR, "sun", "sunny"),
	RAIN(WeatherType.DOWNFALL, "downfall", "rain", "rainy"),
	LIGHTNING(WeatherType.CLEAR, "thunder"),
	STORM(WeatherType.DOWNFALL, "stormy");
	
	private final WeatherType ambient_weather;
	private final String[] aliases;
	
	CustomWeatherType(String... aliases) {
		this(null, aliases);
	}
	
	CustomWeatherType(WeatherType ambient_weather, String... aliases) {
		this.ambient_weather = ambient_weather;
		this.aliases = aliases == null ? new String[0] : aliases;
	}
	
	public static CustomWeatherType get(String name) {
		for (CustomWeatherType type : values()) {
			if (type.name().equalsIgnoreCase(name)) {
				return type;
			}
			for (String alias : type.aliases) {
				if (alias.equalsIgnoreCase(name)) {
					return type;
				}
			}
		}
		throw new IllegalArgumentException("No matching custom weather type: " + name);
	}
	
	public boolean resetToDefault() {
		return ambient_weather == null;
	}
	
	public WeatherType getAmbientWeather() {
		return ambient_weather;
	}
}
