package dev.shortcircuit908.playerweather;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;

public class Utils {
	public static final Pattern UUID_PATTERN =
			Pattern.compile("([0-9a-f]{8})-?([0-9a-f]{4})-?([0-9a-f]{4})-?([0-9a-f]{4})-?([0-9a-f]{12})",
					Pattern.CASE_INSENSITIVE
			);
	public static final Random RANDOM = new Random();
	
	public static OfflinePlayer matchPlayerByName(String name) {
		Player player = Bukkit.getServer().getPlayer(name);
		if (player != null) {
			return player;
		}
		return Bukkit.getServer().getOfflinePlayer(name);
	}
	
	public static OfflinePlayer matchPlayerByNameOrId(String search) {
		try {
			UUID uuid = UUID.fromString(UUID_PATTERN.matcher(search).replaceAll("$1-$2-$3-$4-$5"));
			return Bukkit.getServer().getOfflinePlayer(uuid);
		}
		catch (IllegalArgumentException e) {
			return matchPlayerByName(search);
		}
	}
	
	public static Player matchOnlinePlayerByNameOrId(String search) {
		try {
			UUID uuid = UUID.fromString(UUID_PATTERN.matcher(search).replaceAll("$1-$2-$3-$4-$5"));
			return Bukkit.getServer().getPlayer(uuid);
		}
		catch (IllegalArgumentException e) {
			return Bukkit.getServer().getPlayer(search);
		}
	}
}
