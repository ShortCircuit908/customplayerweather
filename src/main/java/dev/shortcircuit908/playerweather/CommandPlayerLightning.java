package dev.shortcircuit908.playerweather;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CommandPlayerLightning implements CommandExecutor, TabCompleter {
	private final PlayerWeatherPlugin plugin;
	private final String other_permission;
	
	public CommandPlayerLightning(PlayerWeatherPlugin plugin, String permission) {
		this.plugin = plugin;
		this.other_permission = permission + ".other";
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length > 1) {
			sender.sendMessage(ChatColor.RED + "Too many arguments");
			return true;
		}
		
		Player target;
		// Command includes a username or UUID
		if (args.length == 1) {
			target = Utils.matchOnlinePlayerByNameOrId(args[0]);
			if (target == null) {
				sender.sendMessage(String.format(ChatColor.RED + "Player \"%1$s\" not found", args[0]));
				return true;
			}
		}
		else {
			// No username/UUID supplied, so the sender has to be a player
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "Sender is not a player");
				return true;
			}
			target = (Player) sender;
		}
		if (!sender.equals(target) && !sender.hasPermission(other_permission)) {
			sender.sendMessage(ChatColor.RED + "You do not have permission to change another player's weather");
			return true;
		}
		WeatherManager.strikeFakeLightning(target);
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> suggestions = new ArrayList<>();
		if (sender.hasPermission(other_permission)) {
			if (args.length == 1) {
				for (Player player : plugin.getServer().getOnlinePlayers()) {
					if (player.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
						suggestions.add(player.getName());
					}
				}
			}
		}
		return suggestions;
	}
}
