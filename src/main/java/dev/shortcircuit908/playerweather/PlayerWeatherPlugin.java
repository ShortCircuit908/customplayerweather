package dev.shortcircuit908.playerweather;

import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.PluginLoadOrder;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.command.Command;
import org.bukkit.plugin.java.annotation.permission.ChildPermission;
import org.bukkit.plugin.java.annotation.permission.Permission;
import org.bukkit.plugin.java.annotation.plugin.Description;
import org.bukkit.plugin.java.annotation.plugin.LoadOrder;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.author.Author;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Plugin(name = "PlayerWeather", version = "1.0.0")
@Description("Per-player clientside weather effects")
@LoadOrder(value = PluginLoadOrder.STARTUP)
@Author("ShortCircuit908")
@Command(name = "playerweather",
		desc = "Adjust personal weather effects",
		aliases = {"pweather"},
		permission = "playerweather.set.self",
		usage = "/<command> [player] <none|clear|rain|storm|lightning>")
@Command(name = "playerlightning",
		desc = "Summon artificial lightning effects",
		aliases = "plightning",
		permission = "playerweather.smite.self",
		usage = "/<command> [player]")
@Permission(name = "playerweather.set.*",
		desc = "Wildcard playerweather permission",
		defaultValue = PermissionDefault.OP,
		children = {@ChildPermission(name = "playerweather.set.self"),
				@ChildPermission(name = "playerweather.set.other")
		})
@Permission(name = "playerweather.set.self",
		desc = "Set personal weather for yourself",
		defaultValue = PermissionDefault.OP)
@Permission(name = "playerweather.set.other",
		desc = "Set personal weather for others",
		defaultValue = PermissionDefault.OP)
@Permission(name = "playerweather.smite.*",
		desc = "Wildcard playerlightning permission",
		defaultValue = PermissionDefault.OP,
		children = {@ChildPermission(name = "playerweather.smite.self"),
				@ChildPermission(name = "playerweather.smite.other")
		})
@Permission(name = "playerweather.smite.self",
		desc = "Summon artificial lightning for yourself",
		defaultValue = PermissionDefault.OP)
@Permission(name = "playerweather.smite.other",
		desc = "Summon artificial lightning for others",
		defaultValue = PermissionDefault.OP)
public class PlayerWeatherPlugin extends JavaPlugin {
	private final File stored_weather_file = getDataFolder().toPath().resolve("stored_weather.yml").toFile();
	private final YamlConfiguration stored_weather = new YamlConfiguration();
	private final WeatherManager weather_manager = new WeatherManager(this);
	private final RecipeManager recipe_manager = new RecipeManager(this);
	
	@Override
	public void onLoad() {
	
	}
	
	@SuppressWarnings("ConstantConditions")
	@Override
	public void onEnable() {
		// Load plugin config and custom weather store
		try {
			loadConfigs();
		}
		catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
			setEnabled(false);
			return;
		}
		
		// Register commands
		PluginCommand pl_cmd_playerweather = getCommand("playerweather");
		CommandPlayerWeather command_playerweather = new CommandPlayerWeather(this, pl_cmd_playerweather.getPermission());
		pl_cmd_playerweather.setExecutor(command_playerweather);
		pl_cmd_playerweather.setTabCompleter(command_playerweather);
		
		PluginCommand pl_cmd_playerlightning = getCommand("playerlightning");
		CommandPlayerLightning command_playerlightning = new CommandPlayerLightning(this, pl_cmd_playerlightning.getPermission());
		pl_cmd_playerlightning.setExecutor(command_playerlightning);
		pl_cmd_playerlightning.setTabCompleter(command_playerlightning);
		
		// Register event listener
		getServer().getPluginManager().registerEvents(new WeatherListener(this), this);
		
		// In case this is a reload with players online, re-apply weather effects
		getLogger().info("Re-applying custom weather");
		for (String key : stored_weather.getKeys(false)) {
			UUID uuid = UUID.fromString(key);
			Player player = getServer().getPlayer(uuid);
			if (player != null) {
				weather_manager.applyPlayerWeather(player, CustomWeatherType.valueOf(stored_weather.getString(key)));
			}
		}
		
		// Start fake lightning task
		weather_manager.startLightningTask();
		
		// Register weather potion recipes
		if (getConfig().getBoolean("craftable-weather")) {
			getLogger().info("Loading recipes");
			recipe_manager.loadRecipes(getConfig());
		}
	}
	
	public YamlConfiguration getStoredWeatherConfig() {
		return stored_weather;
	}
	
	public WeatherManager getWeatherManager() {
		return weather_manager;
	}
	
	public RecipeManager getRecipeManager() {
		return recipe_manager;
	}
	
	@Override
	public void onDisable() {
		// Stop lightning task
		weather_manager.stopLightningTask();
		
		// Save custom weather store
		try {
			saveConfigs();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void saveConfigs() throws IOException {
		stored_weather.save(stored_weather_file);
	}
	
	private void loadConfigs() throws IOException, InvalidConfigurationException {
		saveDefaultConfig();
		reloadConfig();
		
		if (!stored_weather_file.exists()) {
			stored_weather_file.createNewFile();
		}
		
		stored_weather.load(stored_weather_file);
	}
}
