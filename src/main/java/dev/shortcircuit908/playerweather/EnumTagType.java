package dev.shortcircuit908.playerweather;

import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataType;

public class EnumTagType<E extends Enum<E>> implements PersistentDataType<String, E> {
	private final Class<E> enum_class;
	
	public EnumTagType(Class<E> enum_class) {
		this.enum_class = enum_class;
	}
	
	@Override
	public Class<String> getPrimitiveType() {
		return String.class;
	}
	
	@Override
	public Class<E> getComplexType() {
		return enum_class;
	}
	
	@Override
	public String toPrimitive(Enum complex, PersistentDataAdapterContext context) {
		return complex.name();
	}
	
	@Override
	public E fromPrimitive(String primitive, PersistentDataAdapterContext context) {
		return Enum.valueOf(enum_class, primitive);
	}
}
