package dev.shortcircuit908.playerweather;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class RecipeManager {
	public static final EnumTagType<CustomWeatherType> CUSTOM_WEATHER_TAG = new EnumTagType<>(CustomWeatherType.class);
	public final ItemStack potion_reset = new ItemStack(Material.POTION);
	private final PlayerWeatherPlugin plugin;
	private final NamespacedKey key_custom_weather_type;
	private final NamespacedKey key_recipe_none;
	private final NamespacedKey key_recipe_clear;
	private final NamespacedKey key_recipe_rain;
	private final NamespacedKey key_recipe_lightning;
	private final NamespacedKey key_recipe_storm;
	private final ItemStack potion_clear = new ItemStack(Material.POTION);
	private final ItemStack potion_rain = new ItemStack(Material.POTION);
	private final ItemStack potion_lightning = new ItemStack(Material.POTION);
	private final ItemStack potion_storm = new ItemStack(Material.POTION);
	
	public RecipeManager(PlayerWeatherPlugin plugin) {
		this.plugin = plugin;
		// Derive namespace keys from plugin
		this.key_custom_weather_type = new NamespacedKey(plugin, "custom-weather-type");
		this.key_recipe_none = new NamespacedKey(plugin, "recipe-weather-none");
		this.key_recipe_clear = new NamespacedKey(plugin, "recipe-weather-clear");
		this.key_recipe_rain = new NamespacedKey(plugin, "recipe-weather-rain");
		this.key_recipe_lightning = new NamespacedKey(plugin, "recipe-weather-lightning");
		this.key_recipe_storm = new NamespacedKey(plugin, "recipe-weather-storm");
		
		// Get all potion metadata
		PotionMeta meta_reset = (PotionMeta) potion_reset.getItemMeta();
		PotionMeta meta_clear = (PotionMeta) potion_clear.getItemMeta();
		PotionMeta meta_rain = (PotionMeta) potion_rain.getItemMeta();
		PotionMeta meta_lightning = (PotionMeta) potion_lightning.getItemMeta();
		PotionMeta meta_storm = (PotionMeta) potion_storm.getItemMeta();
		
		// Set persistent custom weather tag
		meta_reset.getPersistentDataContainer()
				.set(key_custom_weather_type, CUSTOM_WEATHER_TAG, CustomWeatherType.NONE);
		meta_clear.getPersistentDataContainer()
				.set(key_custom_weather_type, CUSTOM_WEATHER_TAG, CustomWeatherType.CLEAR);
		meta_rain.getPersistentDataContainer().set(key_custom_weather_type, CUSTOM_WEATHER_TAG,
				CustomWeatherType.RAIN);
		meta_lightning.getPersistentDataContainer()
				.set(key_custom_weather_type, CUSTOM_WEATHER_TAG, CustomWeatherType.LIGHTNING);
		meta_storm.getPersistentDataContainer()
				.set(key_custom_weather_type, CUSTOM_WEATHER_TAG, CustomWeatherType.STORM);
		
		// Apply new potion metadata
		potion_reset.setItemMeta(meta_reset);
		potion_clear.setItemMeta(meta_clear);
		potion_rain.setItemMeta(meta_rain);
		potion_lightning.setItemMeta(meta_lightning);
		potion_storm.setItemMeta(meta_storm);
	}
	
	public NamespacedKey getCustomWeatherTypeKey() {
		return key_custom_weather_type;
	}
	
	@SuppressWarnings("ConstantConditions")
	public void loadRecipes(Configuration configuration) {
		// Get all potion metadata
		PotionMeta meta_reset = (PotionMeta) potion_reset.getItemMeta();
		PotionMeta meta_clear = (PotionMeta) potion_clear.getItemMeta();
		PotionMeta meta_rain = (PotionMeta) potion_rain.getItemMeta();
		PotionMeta meta_lightning = (PotionMeta) potion_lightning.getItemMeta();
		PotionMeta meta_storm = (PotionMeta) potion_storm.getItemMeta();
		
		// Get configuration sections per recipe
		ConfigurationSection section_reset = configuration.getConfigurationSection("weather-recipes.none");
		ConfigurationSection section_clear = configuration.getConfigurationSection("weather-recipes.clear");
		ConfigurationSection section_rain = configuration.getConfigurationSection("weather-recipes.rain");
		ConfigurationSection section_lightning = configuration.getConfigurationSection("weather-recipes.lightning");
		ConfigurationSection section_storm = configuration.getConfigurationSection("weather-recipes.storm");
		
		// Set item names
		meta_reset.setDisplayName(ChatColor.translateAlternateColorCodes('&', section_reset.getString("name")));
		meta_clear.setDisplayName(ChatColor.translateAlternateColorCodes('&', section_clear.getString("name")));
		meta_rain.setDisplayName(ChatColor.translateAlternateColorCodes('&', section_rain.getString("name")));
		meta_lightning.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				section_lightning.getString("name")));
		meta_storm.setDisplayName(ChatColor.translateAlternateColorCodes('&', section_storm.getString("name")));
		
		// Set item lore
		meta_reset.setLore(section_reset.getStringList("lore")
				.stream()
				.map((string) -> ChatColor.translateAlternateColorCodes('&', string))
				.collect(Collectors.toList()));
		meta_clear.setLore(section_clear.getStringList("lore").stream()
				.map((string) -> ChatColor.translateAlternateColorCodes('&', string))
				.collect(Collectors.toList()));
		meta_rain.setLore(section_rain.getStringList("lore").stream()
				.map((string) -> ChatColor.translateAlternateColorCodes('&', string))
				.collect(Collectors.toList()));
		meta_lightning.setLore(section_lightning.getStringList("lore").stream()
				.map((string) -> ChatColor.translateAlternateColorCodes('&', string))
				.collect(Collectors.toList()));
		meta_storm.setLore(section_storm.getStringList("lore").stream()
				.map((string) -> ChatColor.translateAlternateColorCodes('&', string))
				.collect(Collectors.toList()));
		
		// Apply custom metadata
		potion_reset.setItemMeta(meta_reset);
		potion_clear.setItemMeta(meta_clear);
		potion_rain.setItemMeta(meta_rain);
		potion_lightning.setItemMeta(meta_lightning);
		potion_storm.setItemMeta(meta_storm);
		
		// Create recipes
		ShapedRecipe recipe_reset = createRecipe(key_recipe_none, potion_reset, section_reset);
		ShapedRecipe recipe_clear = createRecipe(key_recipe_clear, potion_clear, section_clear);
		ShapedRecipe recipe_rain = createRecipe(key_recipe_rain, potion_rain, section_rain);
		ShapedRecipe recipe_lightning = createRecipe(key_recipe_lightning, potion_lightning, section_lightning);
		ShapedRecipe recipe_storm = createRecipe(key_recipe_storm, potion_storm, section_storm);
		
		// Register recipes
		plugin.getServer().addRecipe(recipe_reset);
		plugin.getServer().addRecipe(recipe_clear);
		plugin.getServer().addRecipe(recipe_rain);
		plugin.getServer().addRecipe(recipe_lightning);
		plugin.getServer().addRecipe(recipe_storm);
	}
	
	@SuppressWarnings("ConstantConditions")
	private ShapedRecipe createRecipe(NamespacedKey key, ItemStack result, ConfigurationSection section) {
		ShapedRecipe recipe = new ShapedRecipe(key, result);
		List<String> shape = section.getStringList("shape");
		
		// Set recipe shape
		recipe.shape(shape.toArray(new String[0]));
		
		// Create set of keys for ingredients
		HashSet<Character> unique_characters = new HashSet<>();
		for (String line : shape) {
			for (char ch : line.toCharArray()) {
				unique_characters.add(ch);
			}
		}
		// Parse ingredients
		for (Character ch : unique_characters) {
			String raw_ingredient = section.getString("ingredients." + ch);
			if (raw_ingredient == null) {
				plugin.getLogger()
						.warning(String.format("Missing ingredient for key %1$s in recipe %2$s", ch, key.getKey()));
				return null;
			}
			// Separate material name and optional potion type
			String[] parts = raw_ingredient.split(";");
			// Parse material
			Material material = Material.getMaterial(parts[0].toUpperCase());
			if (material == null) {
				plugin.getLogger()
						.warning(String.format("Invalid ingredient %1$s for key %2$s in recipe %3$s", parts[0], ch,
								key.getKey()));
				return null;
			}
			// Parse optional potion type
			PotionType potion_type = null;
			if (material.equals(Material.POTION) && parts.length > 1) {
				try {
					potion_type = PotionType.valueOf(parts[1].toUpperCase());
				}
				catch (IllegalArgumentException e) {
					plugin.getLogger()
							.warning(String.format("Invalid potion type %1$s for key %2$s in recipe %3$s", parts[1],
									ch,
									key.getKey()));
					return null;
				}
			}
			// Create ingredient item
			ItemStack ingredient = new ItemStack(material, 1);
			// Apply optional potion type
			if (potion_type != null) {
				PotionMeta meta = (PotionMeta) ingredient.getItemMeta();
				meta.setBasePotionData(new PotionData(potion_type));
				ingredient.setItemMeta(meta);
			}
			// Set ingredient
			recipe.setIngredient(ch, new RecipeChoice.ExactChoice(ingredient));
		}
		return recipe;
	}
}
